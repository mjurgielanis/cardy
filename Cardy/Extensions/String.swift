//
//  String.swift
//  Cardy
//
//  Created by Maciej Jurgielanis on 08.02.2017.
//  Copyright © 2017 Maciej Jurgielanis. All rights reserved.
//

import Foundation

extension String {
	var translated: String {
		return NSLocalizedString(self, comment: "")
	}
}
