//
//  CardViewController.swift
//  Cardy
//
//  Created by Maciej Jurgielanis on 08.02.2017.
//  Copyright © 2017 Maciej Jurgielanis. All rights reserved.
//

import UIKit
import Foundation

class CardsViewController: UIViewController {
	@IBOutlet weak var _cardsBarItem: UITabBarItem!
	@IBOutlet weak var _stackView: UIStackView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		_cardsBarItem.title = "cards".translated
	}
}
