//
//  AddCardViewController.swift
//  Cardy
//
//  Created by Maciej Jurgielanis on 08.02.2017.
//  Copyright © 2017 Maciej Jurgielanis. All rights reserved.
//

import Foundation
import UIKit

class AddCardViewController: UIViewController {
	@IBOutlet weak var _companyField: UITextField!
	override func viewDidLoad() {
		super.viewDidLoad()
		
		_companyField.placeholder = "company_name".translated
	}
	
	
	@IBAction func cancelClicked(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
	}
}
