//
//  CompanyViewController.swift
//  Cardy
//
//  Created by Maciej Jurgielanis on 08.02.2017.
//  Copyright © 2017 Maciej Jurgielanis. All rights reserved.
//

import UIKit
import Foundation

class CompaniesViewController: UIViewController {
	@IBOutlet weak var _companiesBarItem: UITabBarItem!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		_companiesBarItem.title = "companies".translated
	}
}
