//
//  ContactsViewController.swift
//  Cardy
//
//  Created by Maciej Jurgielanis on 08.02.2017.
//  Copyright © 2017 Maciej Jurgielanis. All rights reserved.
//

import Foundation
import UIKit

class ContactsViewController: UIViewController {
	@IBOutlet weak var _contactsBarItem: UITabBarItem!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		_contactsBarItem.title = "contacts".translated
	}
}
